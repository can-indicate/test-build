SHELL = /bin/bash
FLAGS = -O0 -Wall -g

INCLUDE = 
LIBS = 

TARGETDIR = .
# TARGETDIR = ${HOME}
TARGET = ${TARGETDIR}/program
SOURCES = test.cpp
OBJECTS = test.o

all: ${TARGET}

${TARGET}: ${OBJECTS}
	g++ ${FLAGS} -o $@ $^ ${LIBS}

${OBJECTS}: ${SOURCES}
	g++ ${FLAGS} -c $^ ${INCLUDE}

.PHONY = all clean

all: ${TARGET}

clean:
	rm -f *.o ${TARGET}

test: ${TARGET}
	${TARGET}
